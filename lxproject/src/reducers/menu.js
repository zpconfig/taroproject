const MENU_STATE = {
    cataDate:[
        {key:"all",value:"全部"},
        {key:"good",value:"精华"},
        {key:"share",value:"分享"},
        {key:"ask",value:"问答"},
        {key:"job",value:"招聘"},
        {key:"dev",value:"客户端测试"}
    ],
    // 当前分类
    currentCata:{key:"all",value:"全部"},
    showDrawer:false, // 抽屉

}
// menu(prestat=初始值)
export default function menu(prestate=MENU_STATE,action){
    switch(action.type){
        // 显示抽屉分类
        case 'showDrawer':
        return {...prestate,showDrawer:true}
        // 隐藏抽屉
        case 'hideDrawer':
        return {...prestate,showDrawer:false}
        // 点击抽屉 触发切换分类 
        case 'chageCata':
        return {...prestate,currentCata:action.currentCata}
        default:
            return {...prestate}
    }
}