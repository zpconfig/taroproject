import { getTopicList } from './topic'
// 显示抽屉
export function showDrawer(){
    return (dispatch)=>{
        dispatch({type:'showDrawer'})
    }
}
// 关闭抽屉
export function hideDrawer(){
    return (dispatch)=>{
        dispatch({type:'hideDrawer'})
    }
}
// 修改分类名称 
export function changeCata(cata){
    return (dispatch) => {
        dispatch({type:'chageCata',currentCata:cata})
        dispatch(getTopicList({tab:cata.key,page:1,limit:20}))
    }
}