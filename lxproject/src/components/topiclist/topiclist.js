import Taro,{Component} from '@tarojs/taro'
import { View,Text, Button, ScrollView } from '@tarojs/components'
import { getTopicList, getNextList } from '../../actions/topic'
import Topic from './topic'
import {connect} from '@tarojs/redux' 
@connect((store)=>{
    return {...store.topicList,currentCata:store.menu.currentCata}
},(dispatch)=>{
    return {
        getTopicList(params){
            dispatch(getTopicList(params))
        },
        getNextList(params){
            dispatch(getNextList(params))
        }
    }
})
class Topiclist extends Component{
    componentWillMount(){
        let { page,limit,currentCata} = this.props
        this.props.getTopicList&&this.props.getTopicList({page,limit,tab:currentCata.key})
    }
    onScrollToLower(){
        let { page,limit,currentCata} = this.props
        this.props.getNextList&&this.props.getNextList({page:(page+1),limit,tab:currentCata.key})
    }
    render(){
        return(
           <ScrollView 
            scrollY
            style={{height:'650PX'}}
            onScrollToLower = {this.onScrollToLower.bind(this)}
           >
               {this.props.list.map((item)=><Topic item={item}></Topic>)}
           </ScrollView>  
        )
    }
}
export default Topiclist