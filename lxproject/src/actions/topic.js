import { getJson, postJson} from '../utils/request'
import api from '../constants/api'
export function getTopicList(params){
    // console.log(0)
    return async dispatch=>{
      let result = await  getJson(api.gettopics,params)
      if(result&&result.data){
        //   console.log(result.data)
        if(result.data.data){
            dispatch({type:"getTopiclist",list:result.data.data})
        }
      }
    }
}
export function getNextList(params){
    // console.log(0)
    return async dispatch=>{
      let result = await  getJson(api.gettopics,params)
      if(result&&result.data){
        //   console.log(result.data)
        if(result.data.data){
            if(result.data.data.length>0){
                dispatch({type:"appendTopicList",list:result.data.data,page:params.page})
            }
        }
      }
    }
}
// 请求话题列表
export function getTopicInfo(params){
    // console.log(0)
    return async dispatch=>{
      let result = await  getJson(api.gettopicinfo+params.id,params)
      if(result&&result.data&&result.data.success){
        console.log(result.data.data)
         dispatch({type:"getTopicInfo",infoData:result.data.data})
      }else{
          console.error('请求失败')
      }
    }
}