const routPath = 'https://cnodejs.org/api/v1'
const apiObject = {
    gettopics:routPath+'/topics',//获取话题列表
    gettopicinfo: routPath+'/topic/', // 获取话题详情
    checkusertoken: routPath+'/accesstoken',// 验证用户token
    getuserinfo: routPath+'/user/',// 获取用户信息

}
export default apiObject