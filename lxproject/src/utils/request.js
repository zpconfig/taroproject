import Taro from '@tarojs/taro'
// import api from '../constants/api'

export function getJson(url,data){
    return Taro.request({url:url,data:data,method:'GET'})
}
export function postJson(url,data){
    return Taro.request({url:url,data:data,method:'POST'})
}
// 获取话题列表
// export async function getTopicList(){
//     // async 异步函数不会阻塞线程的运行
//     // getJson是promise对象可以用catch捕获信息 
//     let result = await getJson(api.gettopics).catch(message =>{
//         console.log("出错了，错误信息"+message)
//     })
//     // await 等待函数返回结果才会往下运行，让异步的函数像同步那样运行
//     return result
// }