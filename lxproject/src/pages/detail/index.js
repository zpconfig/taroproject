import Taro,{Component} from '@tarojs/taro'
import { View,Text, Button } from '@tarojs/components'
import TopicInfo from '../../components/topicinfo/topicinfo'
import Replie from '../../components/topicinfo/replie'
import { getTopicInfo} from '../../actions/topic'
import {connect} from '@tarojs/redux' 
@connect((store)=>{
    return {topicinfo:store.topicList.topicinfo,replies:store.topicList.replies}
},(dispatch)=>{
    return {
        getTopicInfo(params){
            dispatch(getTopicInfo(params))
        }
    }
})
class Index extends Component{
    config = {
        navigationBarTitleText: '话题详情'
    }
    componentWillMount(){
       let params = {id:this.$router.params.topicid,mdrender:true}
    //    console.log(this.$router.params.topicid) 
       this.props.getTopicInfo&&this.props.getTopicInfo(params)
    }
    render(){
        let {topicinfo,replies} = this.props
        return(
            <View>
              
                <TopicInfo topicinfo={topicinfo}></TopicInfo>
                <Replie replies= {replies}></Replie>
            </View>
        )
    }
}
export default Index