import Taro,{Component} from '@tarojs/taro'
import { View,Text, Button, Image } from '@tarojs/components'
import {connect} from '@tarojs/redux' 
import { showDrawer, changeCata, hideDrawer } from '../../actions/menu'
import { AtDrawer } from 'taro-ui'
import "./menu.less"
@connect((store)=> {
    return {...store.menu}
},(dispatch)=>{
    return {showMenu(){
        // 异步执行 dispatch 可以是一个函数
        dispatch(showDrawer())
    },
    hideMenu(){
        // 异步执行 dispatch 可以是一个函数
        dispatch(hideDrawer())
    },
    changeCata(cata){
        //上面的方法名是自定义的
        // 下面的是action的函数
        dispatch(changeCata(cata))
    }
}
})
class Menu extends Component{
    // 显示抽屉
    showDrawer(){
        // 如果存在就执行，不存在就结束
        this.props.showMenu&&this.props.showMenu();
    }
    changeCatas(index){
        let { cataDate } = this.props
        let clickCata = cataDate[index]
        if(clickCata.key !== this.props.currentCata.key){
            this.props.changeCata&&this.props.changeCata(clickCata)
        }
        
    }
    // 关闭抽屉
    closeDrawer(){
        this.props.hideMenu&&this.props.hideMenu()
    }
    render(){
        let {showDrawer,cataDate} = this.props;
        return(
            <View className="topiclist-menu">
                <AtDrawer style='position:absolute'
                    show={showDrawer}
                    mask
                    onItemClick = {this.changeCatas.bind(this)}
                    onClose = {this.closeDrawer.bind(this)}
                    items = {
                        cataDate.map((item)=>{
                            return item.value
                        })
                    }
                >   
                </AtDrawer>
                <Image onClick={this.showDrawer.bind(this)} className="image" src={require('../../assets/images/cata.png')}></Image>
                <Text>{this.props.currentCata?this.props.currentCata.value:''}</Text>
                <Image className="image" src={require('../../assets/images/login.png')}></Image>
            </View>
        )
    }
}

export default Menu