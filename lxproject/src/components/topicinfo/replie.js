import Taro,{Component} from '@tarojs/taro'
import { View,Text, Button, Image, RichText } from '@tarojs/components'
import {myTimeToLocal} from '../../utils/data'
import './replie.less'
// H5 跟小程序判断
const isweaap = process.env.TARO_ENV === 'weapp'//小程序
class Replie extends Component{
    render(){
        let { replies } = this.props;
        return(
            <View className='topicinfo-replies'>
                {replies.map((item,index)=>{
                    return(
                        <View key={item.id} className='topicinfo-repliy'>
                            <Image className='topicinfo-repliy-image' src={item.author?item.author.avatar_url:''}></Image>
                            <View className='topicinfo-repliy-right'>
                                <View className='topicinfo-repliy-right-body'>
                                    <View className='topicinfo-repliy-right-pie'>
                                    <Text children='loginname'>
                                        {item.author?item.author.loginname:''}
                                    </Text>
                                    <Text children='floor'>
                                        {(index+1)+'楼'}
                                    </Text>
                                    <Text children='time'>
                                        {myTimeToLocal(item.create_at)}
                                    </Text>
                                    </View>
                                    <View className='topicinfo-repliy-right-content'>
                                        {
                                            isweaap? <RichText nodes={item.content}></RichText>:<View dangerouslySetInnerHtml={{_html:item.content}}></View>
                                        }
                                       
                                    </View>
                                </View>
                                <View className='topicinfo-repliy-right-zan'>
                                    <Image className='topicinfo-repliy-img' src={require('../../assets/images/zan.png')}></Image>
                                    <Text>0</Text>
                                    <Image className='topicinfo-repliy-img' src={require('../../assets/images/zhuan.png')}></Image>
                                </View>
                            </View>
                        </View>
                )})}
            </View>
        )
    }
}
export default Replie